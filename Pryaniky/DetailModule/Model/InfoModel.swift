//
//  InfoModel.swift
//  Pryaniky
//
//  Created by Siarhei on 17.07.2020.
//  Copyright © 2020 Siarhei. All rights reserved.
//

import Foundation

struct InfoData {
    var name: String?
    var text: String?
    var url: String?
    var variant: String?
}
