//
//  DetailPresenter.swift
//  MVP
//
//  Created by Siarhei on 15.07.2020.
//  Copyright © 2020 Siarhei. All rights reserved.
//

import Foundation

protocol DetailViewProtocol: class {
    func setInfo(infoData: InfoData?)
}

protocol DetailViewPresenterProtocol: class {
    init(view: DetailViewProtocol, networkService: NetworkSerwiceProtocol, router: RouterProtocol, infoData: InfoData?)
    func setInfo()
    func tap()
}

class DetailPresenter: DetailViewPresenterProtocol {
    weak var view: DetailViewProtocol?
    var router: RouterProtocol?
    let networkService: NetworkSerwiceProtocol!
    var infoData: InfoData?
    
    required init(view: DetailViewProtocol, networkService: NetworkSerwiceProtocol, router: RouterProtocol, infoData: InfoData?) {
        self.view = view
        self.networkService = networkService
        self.infoData = infoData
        self.router = router
    }
    
    func tap() {
        router?.popToRoot()
    }
    
    public func setInfo() {
        self.view?.setInfo(infoData: infoData)
    }
}
