//
//  DetailViewController.swift
//  MVP
//
//  Created by Siarhei on 15.07.2020.
//  Copyright © 2020 Siarhei. All rights reserved.
//

import UIKit

class DetailViewController: UIViewController {

    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var textLabel: UILabel!
    @IBOutlet weak var variantLabel: UILabel!
    @IBOutlet weak var urlLabel: UILabel!

    @IBOutlet weak var pictureView: UIImageView!
    
    var presenter: DetailViewPresenterProtocol!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        presenter.setInfo()
    }
}

extension DetailViewController: DetailViewProtocol {
    func setInfo(infoData: InfoData?) {
        
        nameLabel.text = infoData?.name
        textLabel.text = infoData?.text
        variantLabel.text = infoData?.variant
        urlLabel.text = infoData?.url
        pictureView.downloadedFrom(link: infoData?.url ?? "")
    }
    
}
