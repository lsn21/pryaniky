//
//  DataModel.swift
//  Pryaniky
//
//  Created by Siarhei on 16.07.2020.
//  Copyright © 2020 Siarhei. All rights reserved.
//

import Foundation

struct Variant: Decodable {
    var id: Int
    var text: String
}

struct VariableData: Decodable {
    var text: String?
    var url: String?
    var selectedId: Int?
    var variants: [Variant]?
}

struct DataModel: Decodable {
    var name: String
    var data: VariableData
}

struct RequestResult: Decodable {
    var data: [DataModel]
    let view: [String]
}
