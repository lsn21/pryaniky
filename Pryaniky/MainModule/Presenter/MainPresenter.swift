//
//  MainPresenter.swift
//  Pryaniky
//
//  Created by Siarhei on 16.07.2020.
//  Copyright © 2020 Siarhei. All rights reserved.
//

import Foundation

protocol MainViewProtocol: class {
    func succes()
    func failure(error: Error)
}

protocol MainViewPresenterProtocol: class {
    init(view: MainViewProtocol, networkService: NetworkSerwiceProtocol, router: RouterProtocol)
    func getRequestResult()
    var requestResult: RequestResult? { get set }
    func tapOnTheDescription(dataModel: DataModel?)
    func getDataModelByName(name: String) -> DataModel?
    func getSelectorData() -> [String]?
    func getSelectedId() -> Int
    func getViewType(index: Int) -> String
    func setSelectedId(index: Int)
    func getTextBySelectedId(index: Int) -> String
}

class MainPresenter: MainViewPresenterProtocol {
    
    weak var view: MainViewProtocol?
    var router: RouterProtocol?
    let networkService: NetworkSerwiceProtocol!
    var requestResult: RequestResult?

    required init(view: MainViewProtocol, networkService: NetworkSerwiceProtocol, router: RouterProtocol) {
        self.view = view
        self.networkService = networkService
        self.router = router
        getRequestResult()
    }
    
    func getRequestResult() {
        networkService.getRequestResult { [weak self] result in
            guard let self = self else { return }
            DispatchQueue.main.async {
                switch result {
                case .success(let requestResult):
                    self.requestResult = requestResult
                    self.view?.succes()
                case .failure(let error):
                    self.view?.failure(error: error)
                }
            }
        }
    }
    
    func getViewType(index: Int) -> String {
        return requestResult?.view[index] ?? ""
    }
    
    func getDataModelByName(name: String) -> DataModel? {
        if let dataModels = requestResult?.data {
            for dataModel in dataModels {
                if dataModel.name == name {
                    return dataModel
                }
            }
        }
        return nil
    }
    
    func getSelectorData() -> [String]? {
        if let dataModel = getDataModelByName(name: "selector") {
            let variants = dataModel.data.variants ?? []
            var variantsText = [String]()
            for variant in variants {
                variantsText.append(variant.text)
            }
            return variantsText
        }
        return nil
    }
    
    func getSelectedId() -> Int {
        let dataModel = getDataModelByName(name: "selector")
        return dataModel?.data.selectedId ?? 0
    }
    
    func setSelectedId(index: Int) {
        for i in 0..<(requestResult?.data.count ?? 0) {
            if requestResult?.data[i].name == "selector" {
                requestResult?.data[i].data.selectedId = index
            }
        }
        print(requestResult as Any)
    }
    
    func getTextBySelectedId(index: Int) -> String {
        let dataModel = getDataModelByName(name: "selector")
        return dataModel?.data.variants?[index].text ?? ""
    }
    
    func tapOnTheDescription(dataModel: DataModel?) {
        var infoData = InfoData()
        infoData.name = dataModel?.name
        infoData.text = dataModel?.data.text
        infoData.url = dataModel?.data.url
        if let id = dataModel?.data.selectedId {
            print(id as Any)
            infoData.variant = getTextBySelectedId(index: getSelectedId())
        }
        router?.showDetail(infoData: infoData)
    }
}

