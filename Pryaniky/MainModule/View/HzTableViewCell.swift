//
//  MenuTableCell.swift
//  Matete
//
//  Created by Siarhei on 07.03.2020.
//  Copyright © 2020 Siarhei. All rights reserved.
//

import UIKit

class HzTableViewCell: UITableViewCell {

    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var descriptionLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        selectedBackgroundView = {
            let view = UIView.init()
            view.backgroundColor = UIColor(red: 0.243, green: 0.447, blue: 0.961, alpha: 1)
            return view
        }()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
