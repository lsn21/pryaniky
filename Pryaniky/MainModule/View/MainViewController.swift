//
//  MainViewController.swift
//  Pryaniky
//
//  Created by Siarhei on 16.07.2020.
//  Copyright © 2020 Siarhei. All rights reserved.
//

import UIKit

class MainViewController: UIViewController {

    @IBOutlet weak var tableView: UITableView!
    
    var presenter: MainViewPresenterProtocol!
    private let hzTableViewCell = "HzTableViewCell"
    private let pictureTableViewCell = "PictureTableViewCell"
    private let selectorTableViewCell = "SelectorTableViewCell"

    override func viewDidLoad() {
        super.viewDidLoad()

        tableView.register(UINib(nibName: hzTableViewCell, bundle: nil), forCellReuseIdentifier: hzTableViewCell)
        tableView.register(UINib(nibName: pictureTableViewCell, bundle: nil), forCellReuseIdentifier: pictureTableViewCell)
        tableView.register(UINib(nibName: selectorTableViewCell, bundle: nil), forCellReuseIdentifier: selectorTableViewCell)
    }
    override func viewDidAppear(_ animated: Bool) {
        
        title = presenter.getTextBySelectedId(index: presenter.getSelectedId())
        print(title as Any)
    }
}

extension MainViewController: UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return presenter.requestResult?.view.count ?? 0
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        let viewType = presenter.requestResult?.view[indexPath.row]
        print(viewType as Any)
        switch viewType {
        case "picture":
            return 80.0
        case "selector":
            return 183.0
        default:
            return 64.0
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let viewType = presenter.getViewType(index: indexPath.row)
        switch viewType {
        case "hz":
            if let dataModel = presenter.getDataModelByName(name: viewType) {
                let cell = tableView.dequeueReusableCell(withIdentifier: hzTableViewCell) as! HzTableViewCell
                cell.nameLabel?.text = dataModel.name
                cell.descriptionLabel?.text = dataModel.data.text
                return cell
            }
        case "picture":
            if let dataModel = presenter.getDataModelByName(name: viewType) {
                let cell = tableView.dequeueReusableCell(withIdentifier: pictureTableViewCell) as! PictureTableViewCell
                cell.nameLabel?.text = dataModel.name
                cell.descriptionLabel?.text = dataModel.data.text
                cell.pictureView.downloadedFrom(link: dataModel.data.url ?? "")
                return cell
            }
        case "selector":
            if let dataModel = presenter.getDataModelByName(name: viewType) {
                let cell = tableView.dequeueReusableCell(withIdentifier: selectorTableViewCell) as! SelectorTableViewCell
                cell.nameLabel?.text = dataModel.name
                cell.variantsPickerView.delegate = self
                cell.variantsPickerView.dataSource = self
                cell.selectIndex = presenter.getSelectedId()
                return cell
            }
        default:
            break
        }
        return UITableViewCell()
    }
}

extension MainViewController: UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let viewType = presenter.getViewType(index: indexPath.row)
        if let dataModel = presenter.getDataModelByName(name: viewType) {
            presenter.tapOnTheDescription(dataModel: dataModel)
        }
    }
}

extension MainViewController: MainViewProtocol {
    func failure(error: Error) {
        print(error.localizedDescription)
    }
    
    func succes() {
        tableView.reloadData()
    }
}

extension MainViewController: UIPickerViewDelegate, UIPickerViewDataSource {
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return presenter.getSelectorData()?.count ?? 0
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        return presenter.getSelectorData()?[row]
    }

    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        presenter.setSelectedId(index: row)
        title = presenter.getTextBySelectedId(index: row)
    }
}


