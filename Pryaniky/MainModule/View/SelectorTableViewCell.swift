//
//  SelectorTableViewCell.swift
//  Pryaniky
//
//  Created by SIARHEI LUKYANAU on 7/16/20.
//  Copyright © 2020 Siarhei. All rights reserved.
//

import UIKit

class SelectorTableViewCell: UITableViewCell {

    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var variantsPickerView: UIPickerView!
    var selectIndex = 0

    override func awakeFromNib() {
        super.awakeFromNib()

    }
    
    override func draw(_ rect: CGRect) {
        super.draw(rect)
        
        variantsPickerView.selectRow(selectIndex, inComponent: 0, animated: true)
    }

}
