//
//  ModuleBuilder.swift
//  MVP
//
//  Created by Siarhei on 15.07.2020.
//  Copyright © 2020 Siarhei. All rights reserved.
//

import UIKit

protocol  AssemblerBuilderProtocol  {
    func createMainModule(router: RouterProtocol) -> UIViewController
    func createDetailModule(infoData: InfoData?, router: RouterProtocol) -> UIViewController
}

class AssemblerModelBuilder: AssemblerBuilderProtocol {
    func createMainModule(router: RouterProtocol) -> UIViewController {
        let view = MainViewController()
        let networkService = NetworkSerwice()
        let presenter = MainPresenter(view: view, networkService: networkService, router: router)
        view.presenter = presenter
        
        return view
    }
    
    func createDetailModule(infoData: InfoData?, router: RouterProtocol) -> UIViewController {
        let view = DetailViewController()
        let networkService = NetworkSerwice()
        let presenter = DetailPresenter(view: view, networkService: networkService, router: router, infoData: infoData)
        view.presenter = presenter
        
        return view
    }
}
