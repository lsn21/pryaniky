//
//  NetworkSerwice.swift
//  Pryaniky
//
//  Created by Siarhei on 16.07.2020.
//  Copyright © 2020 Siarhei. All rights reserved.
//

import Foundation

protocol NetworkSerwiceProtocol {
    func getRequestResult(completion: @escaping (Result<RequestResult?, Error>) -> Void)
}

class NetworkSerwice: NetworkSerwiceProtocol {
    func getRequestResult(completion: @escaping (Result<RequestResult?, Error>) -> Void) {
        let urlString = "https://pryaniky.com/static/json/sample.json"
        guard let url = URL(string: urlString) else { return }
        
        URLSession.shared.dataTask(with: url) { data, _, error in
            if let error = error {
                completion(.failure(error))
                return
            }
            do {
                let obj = try JSONDecoder().decode(RequestResult.self, from: data!)
                completion(.success(obj))
            } catch {
                completion(.failure(error))
            }
        }.resume()
    }
}
